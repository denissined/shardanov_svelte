const fs = require('fs');
const fetch = require('sync-fetch')
const posts = fetch('https://devapi.shardanov.com/api/post/read.php', {}).json();

let records = posts['records'];
const BASE_URL = "https://shardanov.com";
let pages = ['blog', 'about', 'blog/category'];

// fs.readdirSync("./src/routes").forEach(file => {
//     file = file.split('.')[0];
//     if (file.charAt(0) !== '_' && file !== "sitemap" && file !== "index" && file !== "login" && file !== "logout") {
//         pages.push(file);
//     }
// });

let render = (pages, records) => `<?xml version="1.0" encoding="UTF-8" ?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
  ${pages
    .map(
        page => `
    <url>
    <loc>${BASE_URL}/${page}/</loc>
    <priority>0.9</priority>
    </url>
  `
    )
    .join("\n")}
  ${records
    .map(
        post => `
    <url>
      <loc>${BASE_URL}/blog/${post.slug}/</loc>
      <priority>0.7</priority>
      <lastmod>${post.created.split(' ')[0]}</lastmod>
    </url>
  `
    )
    .join("\n")}
</urlset>
`;

export function get(req, res, next) {
        res.setHeader("Cache-Control", `max-age=0, s-max-age=${600}`); // 10 minutes
        res.setHeader("Content-Type", "application/rss+xml");

        const sitemap = render(pages, records);
        res.end(sitemap);
    }

