let strings = ['User-agent: *', 'Allow: /', 'Sitemap: https://shardanov.com/sitemap.xml']

let render = (strings) => `${strings.map(stringVal => `${stringVal}`).join("\n")}`;

export function get(req, res, next) {
    res.setHeader("Cache-Control", `max-age=0, s-max-age=${600}`); // 10 minutes
    res.setHeader("Content-Type", " text/plain; charset=utf-8");

    const sitemap = render(strings);
    res.end(sitemap);
}
